#pragma once

#include <vector>

class Rect
{
public:
	Rect(const std::vector<float>& vertices, const std::vector<unsigned int>& indices);
	~Rect();

	void Draw(unsigned int shader);

private:
	unsigned int VAO, VBO, EBO;
};

