#pragma once

#include <vector>

class Triangle
{
public:
	Triangle(const std::vector<float>& vertices);
	~Triangle();

	void Draw(unsigned int shader);

private:
	unsigned int VAO, VBO;
};

