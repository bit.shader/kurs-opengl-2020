#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <cmath>

#include "Triangle.h"
#include "Rect.h"

#include "Shader.h"
#include "Texture.h"

const int WIDTH = 1600;
const int HEIGHT = 900;


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
	else if (key == GLFW_KEY_SPACE) {
		if (action == GLFW_PRESS) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else if (action == GLFW_RELEASE) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

int main() 
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
	if (window == nullptr) {
		std::cerr << "failed to create window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "failed to init GLAD" << std::endl;
		glfwTerminate();
		return -1;
	}

	glViewport(0, 0, WIDTH, HEIGHT);

	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	std::vector<float> vertices {
		-0.5f, -0.5f, 0.0f,
		0.5, -0.5, 0.0f,
		0.0f, 0.5f, 0.0f
	};

	std::vector<float> rectVertices{
	    // position-------| color -----------| texture coords
		 0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 2.0f, 2.0f,  // top right
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 2.0f, -1.0f, // bottom right
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, -1.0f, -1.0f, // bottom left
		-0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 2.0f // top left
	};

	std::vector<unsigned int> rectIndices {
		0, 1, 3,
		1, 2, 3
	};

	auto rect = new Rect(rectVertices, rectIndices);

	auto shader = new Shader("shaders/default.vert", "shaders/default.frag");

	auto tex1 = new Texture("textures/karton.jpg");
	auto tex2 = new Texture("textures/gryfica.png");

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	while (!glfwWindowShouldClose(window)) 
	{
		glClear(GL_COLOR_BUFFER_BIT);

		float t = (float) sin(glfwGetTime()) / 2.0f + 0.5f;

		shader->use();
		shader->setFloat("colorScale", t);

		glActiveTexture(GL_TEXTURE0);
		tex1->Bind();

		glActiveTexture(GL_TEXTURE1);
		tex2->Bind();

		shader->setInt("karton", 0);
		shader->setInt("gryfica", 1);

		rect->Draw(*shader);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	delete rect;
	delete shader;

	glfwTerminate();
	return 0;
}