#pragma once

#include <vector>
#include "Shader.h"

class Rect
{
public:
	Rect(const std::vector<float>& vertices, const std::vector<unsigned int>& indices);
	~Rect();

	void Draw(const Shader& shader);

private:
	unsigned int VAO, VBO, EBO;
};

