#version 420 core

in vec3 color;

out vec4 outColor;
in vec2 texCoords;

uniform sampler2D karton;
uniform sampler2D gryfica;

void main(){
    vec4 k = texture(karton, texCoords);
    vec4 g = texture(gryfica, texCoords);

    outColor = vec4(mix(k.rgb, g.rgb, g.a * 0.6), 1.0);
}