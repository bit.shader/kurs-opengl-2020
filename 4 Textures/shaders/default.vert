#version 420 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoords;

out vec3 color;
out vec2 texCoords;
uniform float colorScale;

void main(){
    texCoords = inTexCoords;
    color = inColor * colorScale;
    gl_Position = vec4(position + vec3(0.0, colorScale - 0.5, 0.0), 1.0);
}