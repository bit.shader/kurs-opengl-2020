#pragma once

#include <vector>
#include "Shader.h"

class Triangle
{
public:
	Triangle(const std::vector<float>& vertices);
	~Triangle();

	void Draw(const Shader& shader);

private:
	unsigned int VAO, VBO;
};

