
#include "Shader.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader(const char *vertexShaderFilePath, const char *fragmentShaderFilePath) {
    auto vertexShader = LoadShaderFile(vertexShaderFilePath, GL_VERTEX_SHADER);
    auto fragmentShader = LoadShaderFile(fragmentShaderFilePath, GL_FRAGMENT_SHADER);

    LinkShader({vertexShader, fragmentShader});

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

Shader::~Shader() {
    glDeleteProgram(id);
}

void Shader::use() const {
    glUseProgram(id);
}

unsigned int Shader::LoadShaderFile(const char *shaderFilePath, GLenum shaderType) {
    std::string code;
    std::ifstream file;

    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try{
        file.open(shaderFilePath);
        std::stringstream stream;
        stream << file.rdbuf();
        code = stream.str();
    } catch (std::ifstream::failure& e){
        std::cerr << "Error reading file " << shaderFilePath << ": " << e.what() << std::endl;
    }

    const char* shaderCode = code.c_str();

    unsigned int shaderId = glCreateShader(shaderType);

    glShaderSource(shaderId, 1, &shaderCode, nullptr);
    glCompileShader(shaderId);

    checkCompileErrors(shaderId);

    return shaderId;

}

void Shader::LinkShader(const std::vector<unsigned int> &shaders) {
    id = glCreateProgram();
    for(auto shader : shaders){
        glAttachShader(id, shader);
    }
    glLinkProgram(id);
    checkLinkErrors(id);
}

void Shader::checkCompileErrors(unsigned int shader) {
    int success;
    char log[1024];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(shader, 1024, nullptr, log);
        std::cerr << "Shader compile error:\n" << log << "\n";
    }
}

void Shader::checkLinkErrors(unsigned int shader) {
    int success;
    char log[1024];
    glGetProgramiv(shader, GL_LINK_STATUS, &success);
    if(!success){
        glGetProgramInfoLog(shader, 1024, nullptr, log);
        std::cerr << "Shader link error:\n" << log << "\n";
    }
}

void Shader::setBool(const std::string &name, bool value) const {
    glUniform1i(glGetUniformLocation(id, name.c_str()), (int) value);
}

void Shader::setInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, float value) const {
    glUniform1f(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::setVec2(const std::string &name, float x, float y) const {
    glUniform2f(glGetUniformLocation(id, name.c_str()), x, y);
}

void Shader::setVec3(const std::string &name, float x, float y, float z) const {
    glUniform3f(glGetUniformLocation(id, name.c_str()), x, y, z);
}

void Shader::setVec4(const std::string &name, float x, float y, float z, float w) const {
    glUniform4f(glGetUniformLocation(id, name.c_str()), x, y, z, w);
}
