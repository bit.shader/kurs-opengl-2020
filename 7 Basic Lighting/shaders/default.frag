#version 420 core

out vec4 outColor;

in vec3 Normal;
in vec3 FragPos;

uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos;

void main(){
    // ambient
    float ambient = 0.1f;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diffuse = max(dot(norm, lightDir), 0.0);

    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float specular = pow( max( dot(viewDir, reflectDir), 0.0 ), 32) * specularStrength;

    vec3 result = (ambient + diffuse + specular) * color * lightColor;
    outColor = vec4(result, 1.0);
}