#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "CubeRenderer.h"
#include "Shader.h"
#include "Texture.h"
#include "Camera.h"

const int WIDTH = 1600;
const int HEIGHT = 900;

Camera camera(glm::vec3(0,0,3));
bool keys[1024];

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
	else if (key == GLFW_KEY_SPACE) {
		if (action == GLFW_PRESS) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else if (action == GLFW_RELEASE) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}
	else {
	    if(action == GLFW_PRESS){
	        keys[key] = true;
	    } else if(action == GLFW_RELEASE){
	        keys[key] = false;
	    }
	}
}

void MoveCamera(Camera* camera, float dt){
    if(keys[GLFW_KEY_W]){
        camera->ProcessMovement(Camera::Direction::Forward, dt);
    }
    if(keys[GLFW_KEY_S]){
        camera->ProcessMovement(Camera::Direction::Backward, dt);
    }
    if(keys[GLFW_KEY_A]){
        camera->ProcessMovement(Camera::Direction::Left, dt);
    }
    if(keys[GLFW_KEY_D]){
        camera->ProcessMovement(Camera::Direction::Right, dt);
    }
}


double lastXPos = 0.0, lastYPos = 0.0;
void processMouseMove(GLFWwindow* window, double xPos, double yPos) {
    lastXPos = xPos;
    lastYPos = yPos;
    camera.ProcessMouseMove(0.0f, 0.0f);

    glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xPos, double yPos){
        double xOffset = xPos - lastXPos;
        double yOffset = lastYPos - yPos;

        lastXPos = xPos;
        lastYPos = yPos;

        camera.ProcessMouseMove(xOffset, yOffset);
    });
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	camera.SetResolution(width, height);
}

int main() 
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
	if (window == nullptr) {
		std::cerr << "failed to create window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "failed to init GLAD" << std::endl;
		glfwTerminate();
		return -1;
	}

	glViewport(0, 0, WIDTH, HEIGHT);

	glEnable(GL_DEPTH_TEST);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, processMouseMove);

	std::vector<float> rectVertices{
	    // position-------| color -----------| texture coords
		 0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 2.0f, 2.0f,  // top right
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 2.0f, -1.0f, // bottom right
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, -1.0f, -1.0f, // bottom left
		-0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 2.0f // top left
	};

	std::vector<unsigned int> rectIndices {
		0, 1, 3,
		1, 2, 3
	};

	auto shader = new Shader("shaders/default.vert", "shaders/default.frag");
	auto lampShader = new Shader("shaders/default.vert", "shaders/lamp.frag");

	auto tex = new Texture("textures/wood.jpg");
	Cube cube(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(1.0f, 0.5f, 0.2f));
	Cube lamp(glm::vec3(1.5f, 1.0f, 1.2f), glm::vec3(0.0f), glm::vec3(0.2f), glm::vec3(1.0f, 1.0f, 1.0f));

	CubeRenderer::Init();

	shader->use();

    camera.SetResolution(WIDTH, HEIGHT);

    float lastFrame = 0.0f;
    float deltaTime = 0.0f;
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	while (!glfwWindowShouldClose(window)) 
	{
	    auto currentFrame = (float) glfwGetTime();
	    deltaTime = currentFrame - lastFrame;
	    lastFrame = currentFrame;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//cube.rotation = (float) glfwGetTime() * glm::vec3(0.5f, 1.0f, 0.0f) * 45.0f;

        MoveCamera(&camera, deltaTime);

        shader->use();
        shader->setVec3("lightPos", lamp.position);
        shader->setVec3("lightColor", lamp.color);

		CubeRenderer::Render(cube, *shader, camera);
		CubeRenderer::Render(lamp, *lampShader, camera);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	CubeRenderer::Cleanup();

	delete tex;
	delete shader;

	glfwTerminate();
	return 0;
}