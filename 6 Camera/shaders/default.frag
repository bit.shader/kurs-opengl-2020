#version 420 core

out vec4 outColor;
in vec2 texCoords;

uniform sampler2D tex;

void main(){
    outColor = texture(tex, texCoords);
}