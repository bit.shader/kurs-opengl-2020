#version 420 core

out vec4 outColor;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos;

uniform sampler2D diffuseTex;
uniform sampler2D specularTex;

void main(){

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diffuseStrength = max(dot(norm, lightDir), 0.0);

    vec3 diffuseColor = texture(diffuseTex, TexCoords).rgb;
    vec3 diffuse = diffuseColor * diffuseStrength;

    // ambient
    vec3 ambient = 0.1 * diffuseColor;

    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float specularStrength = pow( max( dot(viewDir, reflectDir), 0.0 ), 32);

    vec3 specular = texture(specularTex, TexCoords).rgb * specularStrength;

    vec3 result = (ambient + diffuse + specular) * color * lightColor;
    outColor = vec4(result, 1.0);
}