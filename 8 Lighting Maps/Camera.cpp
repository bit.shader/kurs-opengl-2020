

#include <glm/gtc/matrix_transform.hpp>
#include "Camera.h"

void Camera::SetResolution(int w, int h) {
    scrWidth = w;
    scrHeight = h;
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspective(glm::radians(fov), (float) scrWidth / scrHeight, 0.1f, 100.0f);
}

void Camera::ProcessMovement(Direction dir, float dt) {
    float velocity = movementSpeed * dt;

    if(dir == Direction::Forward){
        position += front * velocity;
    } else if(dir == Direction::Backward){
        position -= front * velocity;
    } else if(dir == Direction::Left){
        position -= right * velocity;
    } else if(dir == Direction::Right){
        position += right * velocity;
    }
}

void Camera::ProcessMouseMove(float xOffset, float yOffset) {
    yaw += xOffset * mouseSensitivity;
    pitch += yOffset * mouseSensitivity;

    if(pitch > 89.0f){
        pitch = 89.0f;
    }
    if(pitch < -89.0f){
        pitch = -89.0f;
    }

    UpdateVectors();
}

void Camera::UpdateVectors() {
    glm::vec3 Front;
    float p = glm::radians(pitch);
    float y = glm::radians(yaw);

    Front.x = glm::cos(p) * glm::cos(y);
    Front.y = glm::sin(p);
    Front.z = glm::cos(p) * glm::sin(y);

    front = glm::normalize(Front);

    right = glm::cross(front, worldUp);
    right = glm::normalize(right);

    up = glm::cross(right, front);
    up = glm::normalize(up);
}

Camera::Camera(glm::vec3 position)
: position(position),
  worldUp(0.0f, 1.0f, 0.0f),
  mouseSensitivity(0.25f),
  movementSpeed(3.0f),
  fov(45.0),
  pitch(0.0f),
  yaw(-90.0f){
    UpdateVectors();
}

glm::vec3 Camera::GetPosition() const {
    return position;
}
