#ifndef KURSOPENGL_SHADER_H
#define KURSOPENGL_SHADER_H

#include <glad/glad.h>
#include <vector>

#include <string>
#include <glm/glm.hpp>

class Shader {
public:
    Shader(const char* vertexShaderFilePath, const char* fragmentShaderFilePath);
    ~Shader();

    void use() const;

    void setBool(const std::string& name, bool value) const;
    void setInt(const std::string& name, int value) const;
    void setFloat(const std::string& name, float value) const;
    void setVec2(const std::string& name, float x, float y) const;
    void setVec2(const std::string& name, glm::vec2 v) const;
    void setVec3(const std::string& name, float x, float y, float z) const;
    void setVec3(const std::string& name, glm::vec3 v) const;
    void setVec4(const std::string& name, float x, float y, float z, float w) const;
    void setVec4(const std::string& name, glm::vec4 v) const;
    void setMat4(const std::string& name, glm::mat4 mat) const;
private:
    unsigned int id;

    unsigned int LoadShaderFile(const char* shaderFilePath, GLenum shaderType);
    void LinkShader(const std::vector<unsigned int>& shaders);

    static void checkCompileErrors(unsigned int shader);
    static void checkLinkErrors(unsigned int shader);
};


#endif //KURSOPENGL_SHADER_H
