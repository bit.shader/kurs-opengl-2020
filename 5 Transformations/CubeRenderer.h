
#ifndef OPENGL_PREKURSOR_CUBERENDERER_H
#define OPENGL_PREKURSOR_CUBERENDERER_H

#include <glm/glm.hpp>
#include "Shader.h"
#include "Texture.h"

class Cube {
public:
    Cube(Texture* tex, glm::vec3 pos, glm::vec3 rot, glm::vec3 sc);

    Texture* texture;

    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

    glm::mat4 getModelMatrix() const;
};

class CubeRenderer {
public:
    static void Init();

    static void Render(const Cube& cube, const Shader& shader);

    static void Cleanup();
private:
    static unsigned int VAO, VBO;

    static const std::vector<float> vertices;
};


#endif //OPENGL_PREKURSOR_CUBERENDERER_H
