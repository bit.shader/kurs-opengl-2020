#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "CubeRenderer.h"
#include "Shader.h"
#include "Texture.h"

const int WIDTH = 1600;
const int HEIGHT = 900;


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
	else if (key == GLFW_KEY_SPACE) {
		if (action == GLFW_PRESS) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else if (action == GLFW_RELEASE) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

int main() 
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
	if (window == nullptr) {
		std::cerr << "failed to create window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cerr << "failed to init GLAD" << std::endl;
		glfwTerminate();
		return -1;
	}

	glViewport(0, 0, WIDTH, HEIGHT);

	glEnable(GL_DEPTH_TEST);

	glfwSetKeyCallback(window, key_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	std::vector<float> rectVertices{
	    // position-------| color -----------| texture coords
		 0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 2.0f, 2.0f,  // top right
		 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 2.0f, -1.0f, // bottom right
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, -1.0f, -1.0f, // bottom left
		-0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 2.0f // top left
	};

	std::vector<unsigned int> rectIndices {
		0, 1, 3,
		1, 2, 3
	};

	auto shader = new Shader("shaders/default.vert", "shaders/default.frag");

	auto tex = new Texture("textures/wood.jpg");
	Cube cube(tex, glm::vec3(2.0f, 0.0f, 0.0f), glm::vec3(0.0f), glm::vec3(1.0f));

	CubeRenderer::Init();

	shader->use();
	shader->setMat4("view", glm::translate(glm::mat4(1.0f), glm::vec3(0,0,-3)));
	shader->setMat4("projection", glm::perspective(glm::radians(45.0f), (float)WIDTH/HEIGHT, 0.1f, 100.0f));

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	while (!glfwWindowShouldClose(window)) 
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		cube.rotation = (float) glfwGetTime() * glm::vec3(0.5f, 1.0f, 0.0f) * 45.0f;

		CubeRenderer::Render(cube, *shader);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	CubeRenderer::Cleanup();

	delete tex;
	delete shader;

	glfwTerminate();
	return 0;
}