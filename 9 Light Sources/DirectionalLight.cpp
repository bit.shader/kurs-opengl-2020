
#include "DirectionalLight.h"

#include <utility>

void DirectionalLight::UpdateShader(const Shader &shader) {
    shader.use();

    shader.setVec3(name + ".direction", direction);
    shader.setVec3(name + ".color", color);
}

DirectionalLight::DirectionalLight(std::string name, glm::vec3 direction, glm::vec3 color)
: Light(std::move(name)), direction(direction), color(color){

}
