
#include "SpotLight.h"

void SpotLight::UpdateShader(const Shader &shader) {
    shader.use();

    shader.setVec3(name + ".position", position);
    shader.setVec3(name + ".direction", direction);
    shader.setVec3(name + ".color", color);
    shader.setFloat(name + ".innerCutOff", glm::cos(glm::radians(innerCutOff)));
    shader.setFloat(name + ".outerCutOff", glm::cos(glm::radians(outerCutOff)));
}

SpotLight::SpotLight(std::string name, glm::vec3 position, glm::vec3 direction, glm::vec3 color, float innerCutOff,
                     float outerCutOff) :
                     Light(std::move(name)),
                     position(position),
                     direction(direction),
                     color(color),
                     innerCutOff(innerCutOff),
                     outerCutOff(outerCutOff){

}
