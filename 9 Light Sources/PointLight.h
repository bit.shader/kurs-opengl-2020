
#ifndef KURSOPENGL_POINTLIGHT_H
#define KURSOPENGL_POINTLIGHT_H


#include "Light.h"

class PointLight : public Light{
public:
    PointLight(std::string name, glm::vec3 position, glm::vec3 color, float constant, float linear, float quadratic);

    void UpdateShader(const Shader &shader) override;

    glm::vec3 position, color;

    float constant, linear, quadratic;
};


#endif //KURSOPENGL_POINTLIGHT_H
