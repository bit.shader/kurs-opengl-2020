
#ifndef KURSOPENGL_CAMERA_H
#define KURSOPENGL_CAMERA_H

#include <glm/glm.hpp>

class Camera {
public:
    Camera(glm::vec3 position);

    void SetResolution(int w, int h);

    glm::mat4 GetViewMatrix() const;
    glm::mat4 GetProjectionMatrix() const;
    glm::vec3 GetPosition() const;
    glm::vec3 GetFront() const;

    glm::vec3 backgroundColor;

    enum class Direction{
        Forward, Backward, Left, Right
    };

    void ProcessMovement(Direction dir, float dt);
    void ProcessMouseMove(float xOffset, float yOffset);

private:
    glm::vec3 position;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 front;

    glm::vec3 worldUp;

    unsigned int scrWidth, scrHeight;

    float yaw, pitch;
    float fov;

    float mouseSensitivity;
    float movementSpeed;

    void UpdateVectors();
};

#endif //KURSOPENGL_CAMERA_H
