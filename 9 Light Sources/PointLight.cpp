
#include "PointLight.h"

void PointLight::UpdateShader(const Shader &shader) {
    shader.use();

    shader.setVec3(name + ".position", position);
    shader.setVec3(name + ".color", color);
    shader.setFloat(name + ".constant", constant);
    shader.setFloat(name + ".linear", linear);
    shader.setFloat(name + ".quadratic", quadratic);
}

PointLight::PointLight(std::string name, glm::vec3 position, glm::vec3 color, float constant, float linear,
                       float quadratic)
                       : Light(std::move(name)),
                       position(position),
                       color(color),
                       constant(constant),
                       linear(linear),
                       quadratic(quadratic){

}
