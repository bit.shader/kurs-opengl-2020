
#ifndef KURSOPENGL_LIGHT_H
#define KURSOPENGL_LIGHT_H

#include <string>
#include "Shader.h"

class Light {
public:
    explicit Light(std::string name);

    virtual void UpdateShader(const Shader& shader) = 0;

protected:
    std::string name;
};


#endif //KURSOPENGL_LIGHT_H
