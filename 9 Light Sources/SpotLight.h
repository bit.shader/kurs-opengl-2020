
#ifndef KURSOPENGL_SPOTLIGHT_H
#define KURSOPENGL_SPOTLIGHT_H

#include "Light.h"

class SpotLight : public Light{
public:
    SpotLight(std::string name, glm::vec3 position, glm::vec3 direction, glm::vec3 color, float innerCutOff, float outerCutOff);

    void UpdateShader(const Shader &shader) override;

    glm::vec3 position, direction, color;

    float innerCutOff, outerCutOff;
};

#endif //KURSOPENGL_SPOTLIGHT_H
