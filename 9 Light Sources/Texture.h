
#ifndef KURSOPENGL_TEXTURE_H
#define KURSOPENGL_TEXTURE_H


#include <string>

class Texture {
public:
    Texture(const std::string& fileName);

    void Bind() const;
private:
    unsigned id;

    int width, height, nrChannels;
};


#endif //KURSOPENGL_TEXTURE_H
