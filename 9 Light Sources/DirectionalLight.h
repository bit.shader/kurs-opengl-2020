
#ifndef KURSOPENGL_DIRECTIONALLIGHT_H
#define KURSOPENGL_DIRECTIONALLIGHT_H


#include "Light.h"

class DirectionalLight : public Light{
public:
    DirectionalLight(std::string name, glm::vec3 direction, glm::vec3 color);

    void UpdateShader(const Shader &shader) override;

private:

    glm::vec3 direction, color;

};


#endif //KURSOPENGL_DIRECTIONALLIGHT_H
