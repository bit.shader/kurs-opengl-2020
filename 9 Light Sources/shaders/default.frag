#version 420 core

out vec4 outColor;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

struct DirectionalLight {
    vec3 direction;
    vec3 color;
};

struct PointLight {
    vec3 position;
    vec3 color;

    float constant, linear, quadratic;
};

struct SpotLight {
    vec3 position, direction, color;
    float innerCutOff, outerCutOff;
};

uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos;

uniform sampler2D diffuseTex;
uniform sampler2D specularTex;

uniform DirectionalLight dirLight;

#define MAX_POINT_LIGHTS 4
uniform PointLight pointLights[MAX_POINT_LIGHTS];

uniform SpotLight spotLight;

vec3 calcDirLight(DirectionalLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(-light.direction);

    float diff = max(dot(normal, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);

    vec3 ambient = 0.1 * texture(diffuseTex, TexCoords).rgb;
    vec3 diffuse = diff * texture(diffuseTex, TexCoords).rgb;
    vec3 specular = spec * texture(specularTex, TexCoords).rgb;

    return (ambient + diffuse + specular) * color * light.color;
}

vec3 calcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = max(dot(normal, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);

    vec3 ambient = 0.1 * texture(diffuseTex, TexCoords).rgb;
    vec3 diffuse = diff * texture(diffuseTex, TexCoords).rgb;
    vec3 specular = spec * texture(specularTex, TexCoords).rgb;

    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

    return (ambient + diffuse + specular) * attenuation * color * light.color;
}

vec3 calcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir){
    vec3 lightDir = normalize(light.position - fragPos);

    float diff = max(dot(normal, lightDir), 0.0);

    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);

    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.innerCutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

    vec3 ambient = 0.1 * texture(diffuseTex, TexCoords).rgb;
    vec3 diffuse = diff * texture(diffuseTex, TexCoords).rgb;
    vec3 specular = spec * texture(specularTex, TexCoords).rgb;

    return (ambient + diffuse + specular) * intensity * color * light.color;

}

void main(){
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);

    vec3 result = vec3(0.0f);

    result += calcDirLight(dirLight, norm, FragPos, viewDir);

    for(int i = 0; i < MAX_POINT_LIGHTS; ++i){
        result += calcPointLight(pointLights[i], norm, FragPos, viewDir);
    }

    result += calcSpotLight(spotLight, norm, FragPos, viewDir);

    outColor = vec4(result, 1.0);
}